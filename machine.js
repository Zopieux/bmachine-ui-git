/* config */
var runtime_speed = 400, // delay in ms
    latest_length = 10 // init with this quantity in tape

var code_to_css = {'>': 'gt', '<': 'lt', '*': 'star', 'e': 'e'}

function in_array(arr, obj) {
    return (arr.indexOf(obj) != -1);
}

function Machine(code, tape) {
    this.code = code;
    this.cp = 0;
    this.tape = tape;
    this.index = 0;

    this.step = function() {
        var c = this.code[this.cp];

        if(c == '>') {
            this.index++;
            this.cp++;
        } else if(c == '<') {
            this.index--;
            this.cp++;
        } else if(c == '*') {
            this.tape[this.index] = true;
            this.cp++;
        } else if(c == 'e') {
            if(this.tape[this.index]) {
                this.tape[this.index] = false;
            }
            this.cp++;
        } else {
            var n = parseInt(c);
            if(!isNaN(n) && isFinite(n)) {
                if (this.tape[this.index]) {
                    this.cp = n;
                } else {
                    this.cp++;
                }
            } else {
                displayError("Instruction invalide : " + c);
            }
        }
    };
}

function displayError(msg, timeout) {
    if(timeout == undefined) timeout = 3;
    var box = $('<div class="alert alert-danger" />').text(msg);
    $('#errors').prepend(box);
    box.slideDown().delay(timeout * 1000).fadeOut('slow', function() { $(this).remove(); });
}

function displayMachine(machine) {
    $('#tape li:not(.fool)').remove();
    for (var i = 0; i < machine.tape.length; i++) {
        $('#tape li.fool:first').after(
            $('<li/>')
                .addClass((machine.tape[i]) ? 'one' : 'zero')
                .toggleClass('selected', i == machine.index)
        );
    }
    if(latest_length != machine.tape.length) {
        latest_length = machine.tape.length;
        $('#tape')
            .smoothDivScroll('recalculateScrollableArea')
            .smoothDivScroll('jumpToElement', 'number', machine.index + 1);
    }

    var routes = [];
    var addRoute = function(nfrom, nto) {
        if(nfrom > nto) { var t = nfrom; nfrom = nto; nto = t; } // swap nfrom and nto
        var route_extrema = function(level) {
            var imin = Infinity, imax = 0, lev = routes[level];
            for(var i = 0; i < routes.length; i++) {
                imin = Math.min(imin, lev[i][0]);
                imax = Math.max(imax, lev[i][1]);
            }
            return {min: imin, max: imax};
        }
        var level = 0;
        while(true) {
            var re = route_extrema(level), imin = re.min, imax = re.max;
            if((nfrom < imin && nto < imin ) || (nfrom > imax && nto > imax)) { // klol, can add shit on same level
                if(routes[level] == undefined) routes[level] = [];
                routes[level].push([nfrom, nto]);
                break;
            } else { // ya need to go deeper...
                level++;
            }
        }
        routes[level].sort();
    }

    // render code itself
    $('#code ul li:not(.fool)').remove();
    for (var i = 0; i < machine.code.length; i++) {
        var c = machine.code[i], n = '';
        if(c == '>' || c == '<' || c == '*' || c == 'e') {
            css_class = code_to_css[c];
        } else {
            n = c;
            css_class = 'jump';
        }
        $('#code ul li.fool:first').after(
            $('<li/>').addClass(css_class).text(n)
        );
    }
    // render routes
    for(var level = 0; level < routes.length; level++) {
        var lev = routes[level], begin_at = [], end_at = [];
        var level_obj = $('<ul/>');
        for(var i = 0; i < lev.length; i++) {
            var nfrom = lev[i][0], nto = lev[i][1];
            begin_at.push(nfrom); end_at.push(nto);
        }
        for(var i = 0; i < machine.code.length; i++) {
            var css_class, routing = false;
            if(in_array(begin_at, i)) {
                css_class = 'begin'; routing = true;
            } else if(in_array(end_at, i)) {
                css_class = 'end'; routing = false;
            } else {
                css_class = routing ? 'rout' : 'blank';
            }
            level_obj.append('<li/>').addClass(css_class);
        }
        $('#code-routes').append(level_obj);
    }
}

$(document).ready(function() {
    var machine = new Machine([], new Array(latest_length));
    displayMachine(machine);

    var running = false;

    var tape_div = $('#tape'),
        tape_ul = $('#tape ul');

    $('#tape').smoothDivScroll({
        hotSpotScrolling: false,
        mousewheelScrolling: true,
        manualContinuousScrolling: false,
        scrollToAnimationDuration: 400,
        visibleHotSpotBackgrounds: ''
    });

    $('#code-add').click(function() {
        machine.code = machine.code.concat($('#code-input').val().split(' '));
        displayMachine(machine);
        $('#code-input').val('');
    });

    $('#code-clear').click(function() {
        machine.code = [];
        machine.cp = 0
        displayMachine(machine);
    });

    $('#clear-tape').click(function() {
        machine.tape = new Array(10);
        machine.index = 0;
        displayMachine(machine);
    });

    function doStep() {
        machine.step();
        displayMachine(machine);

        if (machine.cp < machine.code.length) {
            timerId = setTimeout(doStep, runtime_speed);
        } else {
            machine.cp = 0;
            $('#execute-stop').text("Exécuter");
        }
    }

    var timerId = null;
    $('#execute-stop').click(function() {
       if (!timerId) {
           $('#execute-stop').text("Arrêter");
           timerId = setTimeout(doStep, runtime_speed);
       } else {
            clearInterval(timerId);
            timerId = null;
            $('#execute-stop').text("Exécuter");
            if (machine.cp >= machine.code.length) {
                machine.cp = 0;
            }
        }
    });
});
